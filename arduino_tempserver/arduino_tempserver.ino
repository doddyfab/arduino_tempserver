
/*
  An open-source temp server for DS18B20
  Source :   https://www.sla99.fr
  Date : 2018-12-28
*/
#include <Ethernet.h>
#include <DallasTemperature.h>
#include <OneWire.h>


//Gestion web server

byte mac[6] = { 0xBE, 0xEF, 0x00, 0xFD, 0xC7, 0x93 };
char macstr[18];

EthernetServer server(80);

const int LED_NETWORK = A0;
const int LED_CONNECTION = A1;
const int LED_ERROR_BME = A2;
const int LED_OK_BME = A3;
int pin[6]={A0,A1,A2,A3};

void setup()
{
  Serial.begin(9600);
  
  pinMode(LED_CONNECTION, OUTPUT);
  pinMode(LED_NETWORK, OUTPUT);
  pinMode(LED_ERROR_BME, OUTPUT);
  pinMode(LED_OK_BME, OUTPUT);

  snprintf(macstr, 18, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  Serial.print("DHCP : ");
  Serial.println(macstr);
  
  if(!Ethernet.begin(mac)){
    Serial.println("Network failed");
  }
  else{ 
    //Initialisation        
    server.begin();    
    Serial.println(Ethernet.localIP());
    Serial.println(Ethernet.gatewayIP());
    
    digitalWrite(LED_CONNECTION, LOW);    
    digitalWrite(LED_NETWORK, HIGH);    
    digitalWrite(LED_ERROR_BME, LOW);    
    digitalWrite(LED_OK_BME, LOW);
    delay(1000);
    
  }
}

void loop()
{  
  //on déclare le serveur web disponible
  EthernetClient client = server.available();
  //Si un client appelle la page
  if (client) {
    boolean currentLineIsBlank = true;
    //Tant que le client est connecté
    while (client.connected()) {
      if (client.available()) {
        //Allumage led connexion page web
       digitalWrite(LED_CONNECTION, HIGH);                 
       char c = client.read();
       if (c == '\n' && currentLineIsBlank) {  
          //création du code HTML pour générer un JSON
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: application/json");
          client.println("Access-Control-Allow-Origin: *");
          client.println();
          client.print("[{");
            //boucle pour parcourir les ports 2 à 9 sur l'arduino
            for(int i=2;i<10;i++){
              //initialisation 1wire
              OneWire oneWire(i);
              DallasTemperature sensors(&oneWire);
              //on démarre la capture de données du capteur
              sensors.begin();
              sensors.requestTemperatures(); // Send the command to get temperatures
              //on prépare le code json
              client.print("\"TempOneWire");
              client.print(i);
              client.print("\":");
              //si on a -127, c'est que rien n'est connecté
              if(sensors.getTempCByIndex(0) ==-127){
                client.print("\"N/A\"");
                //on affiche la led rouge 300ms
                digitalWrite(LED_ERROR_BME, HIGH); 
                delay(300);
                digitalWrite(LED_ERROR_BME, LOW);
              }
              else{
                //recupération de la température
                client.print(sensors.getTempCByIndex(0));
                //on affiche la led verte 300ms
                digitalWrite(LED_OK_BME, HIGH); 
                delay(300);
                digitalWrite(LED_OK_BME, LOW);
              }
              client.print(",");             
            }
            //vu que l'on est dans une boucle et qu'il faut terminer le json sans virgule, on rajoute une info fixe : ici l'adresse ip
            client.print("\"IP\":");
            client.print("\"");
            client.print(Ethernet.localIP());
            client.print("\"");
            client.print("}]");
            delay(1000);
            //Extinction led connexion page web
            digitalWrite(LED_CONNECTION, LOW); 
            break;          
        }
        if (c == '\n') {
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          currentLineIsBlank = false;
        }         
      }
    }
    delay(1);
    client.stop();   
  }
} 
